/**
* @file  Sirala.cpp
* @description    Sirala sınıfına ait fonksiyon gövdelerini içerir.
* @course 2.Öğretim B Grubu
* @assignment  5-6.Ödev
* @date 01.01.2014
* @author MELİH GÖKSAL,ERAY ALAKESE melih.goksal@ogr.sakarya.edu.tr, eray.alakese@gr.sakarya.edu.tr
*/
#include "Sirala.hpp"
#include <iostream>
#include <fstream>
#include <string>
#include <sstream> //istringstream için
using namespace std;

Sirala::Sirala()
{
	mergeSorted = NULL ;
	source = NULL ;
	sayi =0;
	sayac = 0;
	elemanSayisi = 0;
	sayiAdedi =0;
	satir = 0;
	a = NULL;
}
Agac* Sirala::okuVeOlustur()
{
	ifstream file;
	string i;
	file.open("a.txt");
	
	//satir sayisi hesaplanıp, dinamik nesne oluşturuluyor
	while(getline(file,i))
	{
		sayac++;
	}	
	
	//dosyanın yeniden okunabilmesi için
	file.clear();
	file.seekg(0, ios::beg);
	Heap* agaclar[sayac];	

	while(getline(file,i))
	{	
		
		elemanSayisi = 1;
		for(int t = 0; t <= i.length(); t++ )
		{
			if(i[t] == ' ')
				elemanSayisi++;
		}
		sayiAdedi +=elemanSayisi;
		agaclar[satir] = new Heap(elemanSayisi);

		istringstream stream(i);
		while (stream >>sayi )
		{
			agaclar[satir]->Ekle(sayi);

		}
		satir++;
	}
	file.close();
	mergeSorted = new int[sayiAdedi];

	// OLuşturulan ağaçların yüksekliğini hesapla

	for(int q=0;q<sayac;q++)
	{
		agaclar[q]->yukseklikHesapla();
	}

	// root burada oluşturuluyor.
	 a = new Agac(agaclar[0], sayiAdedi);
	for(int q=1;q<sayac;q++)
	{
		//Her bir agaclar[q] elemanını BST'ye ekle.
		a->ekle(agaclar[q]);
	}
	
	return a;
}
void Sirala::sirala(Agac* a)
{	

	//inorder gezinme basliyor, ekrana basiliyor
	a->inOrder(a->getRoot());
	a->yaz();

	//inorder gezinme sonucu oluşan dizinin adresi aliniyor
	source = a->getInorderDizi();
	
	mergeSort(0, sayiAdedi-1);
	cout<<endl;

	//Merge sort ciktisi
	cout <<"Merge sort uygulandi:";
	for(int r=0;r<sayiAdedi;r++)
		cout<<mergeSorted[r]<<" ";

	cout <<endl;

}

void Sirala::mergeSort(int baslangic, int bitis) 
{
    if (baslangic < bitis) 
    { 
        int orta = (baslangic + bitis) / 2; 
        mergeSort(baslangic, orta); 
        mergeSort(orta + 1, bitis); 
        merge(baslangic, orta + 1, bitis); 
    }  
}
void Sirala::merge(int a, int orta, int b) 
{ 
    int ilk = a; 
    int son = orta - 1; 
    int index = b - a + 1; 
    while ((a <= son) && (orta <= b)) 
    { 
        if (source[a] <= source[orta]) 
        { 
           
            mergeSorted[ilk] = source[a]; 
            ilk++; 
            a++; 
        } 
        else 
        { 
            mergeSorted[ilk] = source[orta]; 
            ilk++; 
            orta++; 
        } 
    } 
    while (a <= son) 
    { 
        mergeSorted[ilk] = source[a]; 
        a++; 
        ilk++; 
    } 
    while (orta <= b) 
    { 
        mergeSorted[ilk] = source[orta]; 
        orta++; 
        ilk++; 
    } 
    for (int i = 0; i < index; i++) 
    { 
        source[b] = mergeSorted[b]; 
        b--; 
    } 
}
Sirala::~Sirala()
{
	
	delete a;
	delete[] mergeSorted, source;
}