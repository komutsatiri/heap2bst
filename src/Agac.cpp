/**
* @file Agac.cpp
* @description    Agac sınıfına ait fonksiyon gövdelerini içerir.
* @course 2.Öğretim B Grubu
* @assignment  5-6.Ödev
* @date 01.01.2014
*@author MELİH GÖKSAL,ERAY ALAKESE melih.goksal@ogr.sakarya.edu.tr, eray.alakese@gr.sakarya.edu.tr
*/
#include "Agac.hpp"
#include "Dugum.hpp"
#include <iostream>
using namespace std;

Agac::Agac(Heap* h,int b)
{

  root = new Dugum(h);
  sayac = b;
  inorderDizi = new int[sayac];

  for(int u =0; u < sayac ;u++)
    inorderDizi[u] = 0;
}
void Agac::ekle(Heap* heap)
{
  
  
  Dugum* yeniDugum = new Dugum(heap);
  Dugum* dugum = root;
  while(dugum != NULL)
  {
    
    if(heap->yukseklikGetir() > dugum->getVeri()->yukseklikGetir())
    {
      // büyükse
      
      if(dugum->getSagdaki()==NULL)
      {
        break;
      }
      else
      {
        dugum = dugum->getSagdaki();
        
      }
    }
    else
    {
      // küçükse
      
      if(dugum->getSoldaki()==NULL)
      {
        break;
      }
      else
      {
        dugum = dugum->getSoldaki();
       
       
      }
    }
  }
  // Bu noktada dugum en uç düğümü tutuyor.

  if(heap->yukseklikGetir() > dugum->getVeri()->yukseklikGetir())
  {
    dugum->setSagdaki(yeniDugum);
  }
  else
  {
    dugum->setSoldaki(yeniDugum);
  }
  

}
int* Agac::inOrder (Dugum* d)
{
    int elemanSayisi;
    int sayac2;
    if (d != NULL)
    {
        inOrder(d->getSoldaki());
        elemanSayisi = d->getVeri()->elemanSayisi();
        sayac2 = 0;
        for(int z=0;z<sayac;z++)
        {
          if(inorderDizi[z] ==0)
            break;
          else
            sayac2++;
        } 
        for(int a=0 ; a < elemanSayisi; a++ )
        {
            inorderDizi[a+sayac2] = d->getVeri()->Getir();
        }
        
        inOrder(d->getSagdaki());
    }
   
}
void Agac::yaz()
{   // İnorder sonrası birleştirilmiş dizi, kısmi sıralanmış
    cout <<"Inorder:";
    for(int o = 0; o < sayac; o++)
    {  
          
          cout << inorderDizi[o]<<" ";

    } 
}
Dugum* Agac::getRoot()
{
  return root;
}
int* Agac::getInorderDizi()
{
  return inorderDizi;

}
Agac::~Agac()
{
  
  //delete root;
  delete[] inorderDizi;
  
}