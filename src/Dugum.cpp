/**
* @file Dugum.cpp
* @description Dugum sınıfına ait fonksiyon gövdelerini içerir.
* @course 2.Öğretim B Grubu
* @assignment  5-6.Ödev
* @date 01.01.2014
*@author MELİH GÖKSAL,ERAY ALAKESE melih.goksal@ogr.sakarya.edu.tr, eray.alakese@gr.sakarya.edu.tr
*/
#include "Dugum.hpp"
#include <iostream>
using namespace std;
Dugum::Dugum()
{
	solDugum = NULL;
	sagDugum = NULL;
	veri = NULL;
}
Dugum::Dugum(Heap* heap)
{
	solDugum = NULL;
	sagDugum = NULL;
	veri = heap;
}
Dugum* Dugum::getSoldaki()
{
	return solDugum;
}
void Dugum::setSoldaki(Dugum* sol)
{
	solDugum = sol;
}
Dugum* Dugum::getSagdaki()
{
	return sagDugum;
}
void Dugum::setSagdaki(Dugum* sag)
{
	sagDugum = sag;
}
Heap* Dugum::getVeri()
{
	return veri;
}
void Dugum::setVeri(Heap* v)
{
	veri=v;
}
Dugum::~Dugum()
{
	delete [] veri;
	delete solDugum, sagDugum;
}
