/**
* @file Heap.cpp
* @description    Heap sınıfına ait fonksiyon gövdelerini içerir.
* @course 2.Öğretim B Grubu
* @assignment  5-6.Ödev
* @date 01.01.2014
*@author MELİH GÖKSAL,ERAY ALAKESE melih.goksal@ogr.sakarya.edu.tr, eray.alakese@gr.sakarya.edu.tr
*/
#include "Heap.hpp"
#include <iostream>
#include <math.h>
using namespace std;
Heap::Heap(int elemanSayisi)
{
	MAX = elemanSayisi;
	dizi = new int[elemanSayisi];
	ES = 0;
}
int Heap::SolCocukGetir(int i)
{
	return 2*i+1;
}
int Heap::SagCocukGetir(int i)
{
	return 2*i+2;
}
int Heap::EbeveynGetir(int i)
{
	return (i-1)/2;
}
bool Heap::Ekle(int e)
{
	if(ES==MAX)
		return false;
	ES++;
	dizi[ES-1] = e;
	Up(ES-1);
	return true;
}
void Heap::Up(int i)
{
	if(i==0)
		return;
	int ebeveyn = EbeveynGetir(i);

	if(dizi[ebeveyn] < dizi[i])
	{
		int temp	= dizi[i];
		dizi[i]		= dizi[ebeveyn];
		dizi[ebeveyn]	= temp;
		Up(ebeveyn);
	}


}
void Heap::Down(int i)
{
	int Sol = SolCocukGetir(i);
	int Sag = SagCocukGetir(i);
	int max;
	if(Sag>=ES)
	{
		if(Sol>=ES)
			return;
		else
			max = Sol;
	}
	else
	{
		if(dizi[Sol] > dizi[Sag])
			max = Sol;
		else
			max = Sag;
	}

	if(dizi[max] > dizi[i])
	{
		int temp = dizi[i];
		dizi[i]	= dizi[max];
		dizi[max] = temp;
		Down(max);
	}	
	
}
void Heap::yukseklikHesapla()
{
	int kalan = ES;
	int basamak = 0;
	while(kalan != 1)
	{
		kalan = kalan/2;
		basamak++;
	}

	yukseklik = basamak;
}
int Heap::yukseklikGetir()
{
	return yukseklik;
}
int Heap::Getir()
{
	int temp;
		if(ES==0)
			return false;
		
		temp = dizi[0];
		dizi[0] = dizi[ES-1];
		ES--;
		Down(0);
	return temp;
}
int Heap::elemanSayisi()
{

	return ES;
}
Heap::~Heap()
{
	cout <<"Heap temizlendi"<<endl;
	delete  dizi;
}
