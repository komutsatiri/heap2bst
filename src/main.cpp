/**
* @file  main.cpp
* @description  Ana fonksiyon bloğu, programın ilk tetiklendiği yer.
* @course 2.Öğretim B Grubu
* @assignment  5-6.Ödev
* @date 01.01.2014
*@author MELİH GÖKSAL,ERAY ALAKESE melih.goksal@ogr.sakarya.edu.tr, eray.alakese@gr.sakarya.edu.tr
*/
#include <iostream>
#include "Sirala.hpp"
#include "Heap.hpp"
using namespace std;
int main()
{
	Agac* gecici;
	Sirala *s1 = new Sirala();
	gecici = s1->okuVeOlustur();
	s1->sirala(gecici);
	delete s1;
	return 0;

}