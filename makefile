all: compile execute

compile:
	g++ -I ./include/ -o ./lib/Sirala.o -c ./src/Sirala.cpp
	g++ -I ./include/ -o ./lib/Heap.o -c ./src/Heap.cpp
	g++ -I ./include/ -o ./lib/Agac.o -c ./src/Agac.cpp
	g++ -I ./include/ -o ./lib/Dugum.o -c ./src/Dugum.cpp
	g++ -I ./include/ -o ./bin/app ./lib/Heap.o ./lib/Agac.o ./lib/Dugum.o ./lib/Sirala.o ./src/main.cpp

execute:
	./bin/app
remove:
	rm -rf ./bin/* ./lib/*