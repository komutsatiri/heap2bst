/**
* @file  Agac.hpp
* @description  Agac sınıfına ait fonksiyon prototiplerini ve sınıf değişkenlerini içerir.
* @course 2.Öğretim B Grubu
* @assignment  5-6.Ödev
* @date 01.01.2014
*@author MELİH GÖKSAL,ERAY ALAKESE melih.goksal@ogr.sakarya.edu.tr, eray.alakese@gr.sakarya.edu.tr
*/
#ifndef AGAC_HPP
#define AGAC_HPP
#include "Dugum.hpp"
class Agac
{
private:
	Dugum* root;
	int sayac;
	int* inorderDizi;
public:
	Agac(Heap* h,int);
	~Agac();
	void ekle(Heap* heap);	
	int* inOrder(Dugum* d);
	Dugum* getRoot();
	int* getInorderDizi();
	void yaz();
	
};
#endif
