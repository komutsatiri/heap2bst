/**
* @file Heap.hpp
* @description Sirala sınıfına ait fonksiyon prototiplerini ve sınıf değişkenlerini içerir.
* @course 2.Öğretim B Grubu
* @assignment  5-6.Ödev
* @date 01.01.2014
*@author MELİH GÖKSAL,ERAY ALAKESE melih.goksal@ogr.sakarya.edu.tr, eray.alakese@gr.sakarya.edu.tr
*/
#ifndef HEAP_HPP
#define HEAP_HPP
#include <iostream>
class Heap
{
public:
			Heap(int elemanSayisi);
			~Heap();
	int		SolCocukGetir(int i);
	int		SagCocukGetir(int i);
	int		EbeveynGetir(int i);
	bool	Ekle(int e);
	int		Getir();
	void 	yukseklikHesapla();
	int 	yukseklikGetir();
	int 	elemanSayisi();
private:
	int		*dizi;
	int 	MAX;
	int		ES;
	int 	yukseklik;
	void	Up(int i);
	void	Down(int i);
};

#endif