/**
* @file Sirala.hpp
* @description Sirala sınıfına ait fonksiyon prototiplerini ve sınıf değişkenlerini içerir.
* @course 2.Öğretim B Grubu
* @assignment  5-6.Ödev
* @date 01.01.2014
* @author MELİH GÖKSAL,ERAY ALAKESE melih.goksal@ogr.sakarya.edu.tr, eray.alakese@gr.sakarya.edu.tr
*/
#ifndef SIRALA_HPP
#define SIRALA_HPP
#include "Heap.hpp"
#include "Agac.hpp"
class Sirala
{
private:
	int* mergeSorted;
	int* source;
	Heap*	agaclar;
	Agac* a;
	int sayi,sayac,elemanSayisi,sayiAdedi,satir,;

public:
		  Sirala();
		  ~Sirala();
	void  sirala(Agac*);	  
	Agac* okuVeOlustur();	  
	void  mergeSort(int, int);
	void  merge(int ,int , int);


};


#endif