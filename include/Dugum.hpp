/**
* @file Dugum.hpp
* @description Dugum sınıfına ait fonksiyon prototiplerini ve sınıf değişkenlerini içerir.
* @course 2.Öğretim B Grubu
* @assignment  5-6.Ödev
* @date 01.01.2014
*@author MELİH GÖKSAL,ERAY ALAKESE melih.goksal@ogr.sakarya.edu.tr, eray.alakese@gr.sakarya.edu.tr
*/
#ifndef DUGUM_HPP
#define DUGUM_HPP
#include "Heap.hpp"
class Dugum
{
private:
	Heap* veri;
	Dugum* solDugum;	
	Dugum* sagDugum;
public:
	
	Dugum();
	Dugum(Heap* heap);
	~Dugum();
	Dugum* getSoldaki();
	Dugum* getSagdaki();
	void setSoldaki(Dugum* sol);
	void setSagdaki(Dugum* sag);
	Heap* getVeri();
	void setVeri(Heap* veri);

};
#endif